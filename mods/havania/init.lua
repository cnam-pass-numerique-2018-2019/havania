  local pos1 = {x=152,y=6,z=978}                                                -- Determine une position de spawn possible pour les joueur
                                          

minetest.register_on_respawnplayer(function(player)
  player:set_pos(pos2)
  return true
end)
  
minetest.register_on_joinplayer(function(player)                                  -- Fonction pour vezrifier les joueurs
player:set_pos(pos1)     
local name = player:get_player_name()                                           -- Recupere le nom des joueurs  
if name == "Shoggoth" then                                                      -- Verifie si le nom du joueur est Shoggoth
                                                           -- Si le nom correspnd, le joueur est mis a la pos1
    minetest.chat_send_player(name,"Dégage trou d'cul "..name)                             -- Et envoie le message "Dégage trou d'cul"
  else                                                                            -- Sinon
    player:set_pos(pos2)                                                          -- Envoi le joueur à la pos2
    minetest.chat_send_player(name,"Bienvenue"..name)              -- Et ce second message est envoyer
    end 
end)


minetest.register_node("havania:MarkOfSacrifice", {                                 -- Nom unique du block
    description = "Mark",                                                             -- La description s'affiche quand on survole l'item dans l'inventaire
    tiles = {"mark.png"},                                                               -- Nom du fichier dans le dossier
    groups = {cracky=3, stone=1},                                                         -- Temps de destruction du block et son type
    on_rightclick = function(pos, node, player, itemstack, pointed_thing)                   -- Evenement qui active la fonction du block
      local name = player:get_player_name()                                                  -- Recupere le nom du joueur qui active le block
      local health = player:get_hp()                                                          -- Recupere le nombre de point de vie du joueur
    end,
    on_construct = function(pos)                                    -- Debut de la fonction
			local meta = minetest.get_meta(pos)
			meta:set_string("formspec", "field[text;;${text}]")
		end,

    on_receive_fields = function(pos, formname, fields, player)     -- Code pour le formulaire
        print("fields : "..dump(fields))
        local joueurs = minetest.get_connected_players()            -- Recuperation des noms des joueurs sur le serveur
        local physics = {speed = 0.3, gravity = 1, jump = 1}        -- Modifie les parametres physique du joueur
        local exist = false
        local plname = player:get_player_name()
        for i,v in ipairs(joueurs) do                               -- Recherche tous les noms de joueur
          local name = v:get_player_name()
          if fields.text == name then                               -- Verifie le nom entrée dans le formulaire
            player:set_physics_override(physics)                    -- Applique les changements de la ligne 30
            v:set_hp(0)                                             -- met les point de vie du joueur visé a 0
            exist = true
        end
        if exist == false then
          minetest.chat_send_player(plname,"Tu t'es trompé... Tu vas mourir...")
          minetest.after(6, function()
              minetest.chat_send_player(plname," on t'avait prévenu...")
              end)
          minetest.after(10,function()
              player:set_hp(0)
            end)
        end
      end
    end,
    
    can_dig = function(pos,player)                                 -- empeche la destruction hors du mode creatif
      local name = player:get_player_name()
      if minetest.check_player_privs(name, {creative = true}) then
        return true
      else
        return false
      end
    end,
})
  
minetest.register_node("havania:unicorn", {               -- Chemin du fichier dans le mod
    description = "Defense",                                                   -- Nom du block dans le jeu
    tiles = {"mimi.png"},                                                    -- Nom du fichier dans le dossier
    groups = {cracky=3, stone=1},                                                -- Temps de destruction du block et son type
    on_punch = function(pos,node,player,pointed_thing)
      local interrupteur = math.random(4)                         -- Choisi un nombre entre 1 et 4
      if interrupteur == 1 then                                  -- Si 1 les points de vie sont mis a 1
        player:set_hp(1) 
      elseif interrupteur == 2 then                              -- Si 2 les attributs physique sont changées
        player:set_physics_override({speed = 4, gravity = 1, jump = 1})
      elseif interrupteur == 3 then                              -- Si 3 les attributs physique sont changées d'une autre maniere
        player:set_physics_override({jump = 2, gravity = 0.3, speed=6})
      elseif interrupteur == 4 then                              -- Si 4 autre changement des attributs physique
        player:set_physics_override({jump = 2, gravity = 3, speed = 2})
      end
      local name = player:get_player_name()
      minetest.chat_send_player(name,"your physics are now : "..dump(player:get_physics_override()))
    end,
    can_dig = function(pos,player)                              -- Determine si le block peut etre detruit hors du mode creatif
      local name = player:get_player_name()
      if minetest.check_player_privs(name, {creative = true}) then
        return true
      else
        return false
      end
    end,

})